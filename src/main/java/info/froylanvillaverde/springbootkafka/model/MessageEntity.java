package info.froylanvillaverde.springbootkafka.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author Froylan Villaverde
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data//This will provide getter, setter ...
public class MessageEntity {

    private String name;
    private LocalDateTime time;
}
